const {BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function crearVentanaNuevaTarea(){
    ventanaNuevaTarea= new BrowserWindow({
      width: 800,
      height: 250,
      title:"Nueva Tarea",
      webPreferences: {
        nodeIntegration: true
      },
  
    });
    ventanaNuevaTarea.setMenu(null);
  
    let urlVentana =url.format({
      pathname: path.join(__dirname, '../views/nuevaTarea.html'),
      protocol: "file",
      slashes: true
  });
  ventanaNuevaTarea.loadURL(urlVentana)
  
  ventanaNuevaTarea.on("closed", ()=>{
    ventanaNuevaTarea =null;
  });
  return ventanaNuevaTarea;
  }
  module.exports={crearVentanaNuevaTarea}