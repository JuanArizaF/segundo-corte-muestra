document.addEventListener("DOMContentLoaded", () => {
    const{ipcRenderer} = require('electron');
    // Obtiene el boton
    let boton = document.getElementById("boton")
    // Click en el boton de crear tarea
    boton.addEventListener("click", (evento) => {
        // Previene el comportamiento por defecto
        evento.preventDefault();
        // Obtiene el texto
        let inputTarea = document.getElementById("tarea");
        // Obtiene el texto digitado
        let tarea = inputTarea.value;
        // Crea objeto de datos que se enviaran
        const datos = {
            tarea: tarea
        }
        console.log(datos);
        alert(datos[tarea])
        ipcRenderer.send("nueva-tarea",datos)

    })

});
