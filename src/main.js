const { app, BrowserWindow , Menu , ipcMain } = require("electron");
const url = require("url");
const path = require("path");
const {crearVentanaNuevaTarea} =require('./templates/nuevaTarea');

// Electron reload en modo desarrollo
if (process.env.NODE_ENV !== "production") {
  require("electron-reload")(__dirname, {});
}
let ventanaNuevaTarea;
let ventanaPrincipal;
app.on("ready", () => {
  // Crea la ventan principal
  ventanaPrincipal = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
    },

  });

 //Crear URL ventana
 let urlVentana =url.format({
     pathname: path.join(__dirname, 'views/index.html'),
     protocol: "file",
     slashes: true
 });
 const menuPrincipal = Menu.buildFromTemplate(templateMenu);
 Menu.setApplicationMenu(menuPrincipal)
 //crea la ventana cargando html
  ventanaPrincipal.loadURL(urlVentana)

  ventanaPrincipal.on("closed", ()=>{
    app.quit();
  });

  ipcMain.on("nueva-tarea",(evento,datos)=>{
    console.log(datos);
    ventanaPrincipal.webContents.send("nueva-tarea",datos );
    ventanaNuevaTarea.close();
  });

});


const templateMenu =[
  {
    label: "Tareas",
    submenu:[
      {
        label: "Nueva Tarea",
        accelerator :"Ctrl+N",
        click(){
          console.log("Click");
          //crear la ventana de crear tarea
          ventanaNuevaTarea=crearVentanaNuevaTarea();
        }
      },
      {
        label:"Salir",
        accelerator: process.platform ==="darwin"? 'comand+Q': "Ctrl+Q",
        click(){
          app.quit();
        }
      }
    ]
  }
]

// Verifica el sistema operativo
if (process.platform === "darwin"){
  templateMenu.unshift({label: app.getName()});
}
if (process.env.NODE_ENV !== "production") {
  templateMenu.push({
    label: "DevTools",
    submenu: [
      {
        label: "Mostrar u ocultar Dev Tools",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
      {
        role: "reload",
      },
    ],
  });
}


